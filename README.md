# Volume Calculation Tools

## Environment
In order to use these tools, you need the OSGeo Library (Includes GDAL and others).
It's tricky to install, so we have it set up on Hamilton. 
1. `ssh [username]@hopper.cluster.earlham.edu` to log into Hopper your machine.
2. `ssh [username]@hamilton.cluster.earlham.edu` to log into Hamilton from Hopper.
3. `git clone` this repository into your home directory.
4. `module load python/gdal` to load the OSGeo libraries and Python 3.11
5. `python volume_from_dem.py -d [DEM FILE] -f [FLOOR ELEVATION]` to run the program on a DEM file.

## Getting a DEM
1. Log in to WebODM, and find the project you want to analyse. Under the download assets dropdown, there is a link for a "Terrain Model" and a "Surface Model", we usually want the Surface Model (DSM), but they can both be used as DEMs. 
2. Right click on the "Surface Model" option in the drop down, and click "copy link".
3. In the git repository you cloned in the first section, use the command `wget [URL]` to download a file to the directory, where [URL] is the link you just copied from WebODM. 

